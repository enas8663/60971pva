<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Films extends Migration
{
	public function up()
	{

        if (!$this->db->tableexists('hall'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id зала'),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE,
                    'comment' => 'наименование')
            ));

            // create table
            $this->forge->createtable('hall', TRUE);
        }

        if (!$this->db->tableexists('film'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id'),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE,
                    'comment' => 'наименование'),
                'time' => array(
                    'type' => 'TIME',
                    'null' => FALSE,
                    'comment' => 'продолжительность')
            ));

            // create table
            $this->forge->createtable('film', TRUE);
        }

        if (!$this->db->tableexists('session'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id'),
                'id_hall' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id зала'),
                'id_film' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id фильм'),
                'date' => array(
                    'type' => 'datetime',
                    'null' => FALSE,
                    'comment' => 'дата и время начала')
            ));

            $this->forge->addForeignKey('id_hall','hall','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_film','film','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('session', TRUE);
        }

        if (!$this->db->tableexists('place'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id'),
                'id_hall' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id зала'),
                'row' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'ряд'),
                'count' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'номер'),
                'price_category' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE,
                    'comment' => 'ценовая категория')
            ));

            $this->forge->addForeignKey('id_hall','hall','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('place', TRUE);
        }

        if (!$this->db->tableexists('ticket'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id'),
                'id_session' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id сеанса'),
                'id_seat' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id места'),
                'full_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE,
                    'comment' => 'ФИО')
            ));

            $this->forge->addForeignKey('id_session','session','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_seat','place','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('ticket', TRUE);
        }

        if (!$this->db->tableexists('price'))
        {
            // Setup keys
            $this->forge->addkey('id', TRUE);

            // Build Schema
            $this->forge->addfield(array(
                'id' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE,
                    'comment' => 'id'),
                'id_session' => array(
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'comment' => 'id сеанса'),
                'price_category' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE,
                    'comment' => 'ценовая категория'),
                'price' => array(
                    'type' => 'float',
                    'null' => FALSE,
                    'comment' => 'цена')
            ));

            $this->forge->addForeignKey('id_session','session','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('price', TRUE);
        }

	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->droptable('ticket');
        $this->forge->droptable('session');
        $this->forge->droptable('price');
        $this->forge->droptable('place');
        $this->forge->droptable('hall');
        $this->forge->droptable('film');
	}
}
