<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Adduserid extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('film'))
        {
            $this->forge->addColumn('film',array(
                'userid' => array('type' => 'INT', 'null' => TRUE)
            ));
        }
    }
    public function down()
    {
        $this->forge->dropColumn('film', 'userid');
    }
}
