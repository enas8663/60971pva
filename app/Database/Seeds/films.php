<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class films extends Seeder
{
    public function run()
    {
        //film
        $data = [
            'name' => 'Семейка Крудс: новоселье',
            'time' => '01:35:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Обратная связь',
            'time' => '01:35:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Обратная связь',
            'time' => '01:37:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Звонок. Последняя глава',
            'time' => '01:39:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Проклятие Лауры. Завещание',
            'time' => '01:37:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Брешь',
            'time' => '01:32:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Про Лёлю и Миньку',
            'time' => '01:25:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Самый Новый год!',
            'time' => '01:20:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Серебряные коньки',
            'time' => '02:16:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Неадекватные люди 2',
            'time' => '02:15:00'
        ];
        $this->db->table('film')->insert($data);

        $data = [
            'name' => 'Творцы снов',
            'time' => '01:21:00'
        ];
        $this->db->table('film')->insert($data);

        //hall
        $data = [
            'name' => 'зал 1',
        ];
        $this->db->table('hall')->insert($data);

        $data = [
            'name' => 'зал 2',
        ];
        $this->db->table('hall')->insert($data);

        $data = [
            'name' => 'зал 3',
        ];
        $this->db->table('hall')->insert($data);

        $data = [
            'name' => 'зал 4',
        ];
        $this->db->table('hall')->insert($data);

        $data = [
            'name' => 'бол. зал',
        ];
        $this->db->table('hall')->insert($data);

        $data = [
            'name' => '4DX',
        ];
        $this->db->table('hall')->insert($data);

        //session
        $data = [
            'id_hall' => 1,
            'id_film' => 1,
            'date' => '2020-12-20 10:35:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 1,
            'date' => '2020-12-20 12:35:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 1,
            'date' => '2020-12-21 08:37:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 1,
            'date' => '2020-12-21 13:12:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 3,
            'id_film' => 2,
            'date' => '2020-12-20 12:44:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 4,
            'id_film' => 2,
            'date' => '2020-12-27 14:39:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 1,
            'id_film' => 2,
            'date' => '2020-12-28 15:40:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 2,
            'date' => '2020-12-20 11:40:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 1,
            'id_film' => 3,
            'date' => '2020-12-24 09:30:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 3,
            'date' => '2020-12-24 10:40:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 3,
            'date' => '2020-12-23 12:18:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 4,
            'id_film' => 3,
            'date' => '2020-12-26 09:43:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 3,
            'id_film' => 4,
            'date' => '2020-12-22 12:30:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 3,
            'id_film' => 4,
            'date' => '2020-12-22 14:45:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 4,
            'date' => '2020-12-26 13:46:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 4,
            'date' => '2020-12-20 15:03:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 4,
            'id_film' => 5,
            'date' => '2021-01-13 09:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 5,
            'date' => '2021-01-07 10:35:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 5,
            'date' => '2021-01-11 13:35:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 4,
            'id_film' => 5,
            'date' => '2020-12-30 12:45:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 6,
            'date' => '2021-01-11 07:10:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 3,
            'id_film' => 6,
            'date' => '2021-01-10 11:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 7,
            'date' => '2021-01-12 09:15:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 7,
            'date' => '2021-01-12 14:50:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 8,
            'date' => '2020-12-17 07:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 1,
            'id_film' => 8,
            'date' => '2020-12-17 20:17:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 2,
            'id_film' => 9,
            'date' => '2020-12-18 22:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 9,
            'date' => '2020-12-18 23:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 10,
            'date' => '2020-12-25 19:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 3,
            'id_film' => 10,
            'date' => '22020-12-25 21:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 5,
            'id_film' => 8,
            'date' => '2021-01-16 13:00:00'

        ];
        $this->db->table('session')->insert($data);

        $data = [
            'id_hall' => 6,
            'id_film' => 3,
            'date' => '2021-01-03 21:00:00'

        ];
        $this->db->table('session')->insert($data);

        //place
        $data = [
            'id_hall' => 1,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '3',
            'count' => '11',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '3',
            'count' => '12',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '3',
            'count' => '13',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '3',
            'count' => '14',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 1,
            'row' => '3',
            'count' => '15',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '3',
            'count' => '11',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '3',
            'count' => '12',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '3',
            'count' => '13',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '3',
            'count' => '14',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 2,
            'row' => '3',
            'count' => '15',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '3',
            'count' => '11',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '3',
            'count' => '12',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '3',
            'count' => '13',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '3',
            'count' => '14',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 3,
            'row' => '3',
            'count' => '15',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 4,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Дешевый'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '11',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '12',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '13',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '14',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '15',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '16',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '17',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '18',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '19',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 5,
            'row' => '3',
            'count' => '20',
            'price_category' => 'Стандарт'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '1',
            'count' => '1',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '1',
            'count' => '2',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '1',
            'count' => '3',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '1',
            'count' => '4',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '1',
            'count' => '5',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '2',
            'count' => '6',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '2',
            'count' => '7',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '2',
            'count' => '8',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '2',
            'count' => '9',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        $data = [
            'id_hall' => 6,
            'row' => '2',
            'count' => '10',
            'price_category' => 'Дорогой'

        ];
        $this->db->table('place')->insert($data);

        //ticket
        $data = [
            'id_session' => 1,
            'id_seat' => 37,
            'full_name' => 'Иванов Иван Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 1,
            'id_seat' => 38,
            'full_name' => 'Иванов Иван Петрович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 1,
            'id_seat' => 19,
            'full_name' => 'Иванов Иван Константинович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 1,
            'id_seat' => 13,
            'full_name' => 'Иванов Иван Александрович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 1,
            'id_seat' => 60,
            'full_name' => 'Иванов Иван Сергеевич'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 2,
            'id_seat' => 76,
            'full_name' => 'Иванов Петр Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 2,
            'id_seat' => 19,
            'full_name' => 'Иванов Петр Сергеевич'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 2,
            'id_seat' => 56,
            'full_name' => 'Иванов Петр Петрович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 2,
            'id_seat' => 46,
            'full_name' => 'Иванов Петр Александрович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 2,
            'id_seat' => 13,
            'full_name' => 'Иванов Петр Николаевич'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 3,
            'id_seat' => 59,
            'full_name' => 'Иванов Сергей Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 3,
            'id_seat' => 57,
            'full_name' => 'Иванов Сергей Сергеевич'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 7,
            'id_seat' => 57,
            'full_name' => 'Иванов Сергей Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 7,
            'id_seat' => 12,
            'full_name' => 'Иванов Сергей Алексеевич'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 13,
            'id_seat' => 14,
            'full_name' => 'Иванов Константин Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 26,
            'id_seat' => 59,
            'full_name' => 'Иванов Иван Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 32,
            'id_seat' => 29,
            'full_name' => 'Иванов Сергей Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 26,
            'id_seat' => 12,
            'full_name' => 'Иванов Петр Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 23,
            'id_seat' => 12,
            'full_name' => 'Иванов Сергей Иванович'

        ];
        $this->db->table('ticket')->insert($data);

        $data = [
            'id_session' => 8,
            'id_seat' => 4,
            'full_name' => 'Иванов Сергей Сергеевич'

        ];
        $this->db->table('ticket')->insert($data);

        //price
        $data = [
            'id_session' => 1,
            'price_category' => 'Дешевый',
            'price' => '100'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 1,
            'price_category' => 'Дорогой',
            'price' => '200'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 2,
            'price_category' => 'Дешевый',
            'price' => '100'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 2,
            'price_category' => 'Стандарт',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 3,
            'price_category' => 'Дорогой',
            'price' => '500'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 4,
            'price_category' => 'Дорогой',
            'price' => '600'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 5,
            'price_category' => 'Стандарт',
            'price' => '350'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 6,
            'price_category' => 'Дорогой',
            'price' => '700'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 7,
            'price_category' => 'Стандарт',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 7,
            'price_category' => 'Дорогой',
            'price' => '650'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 8,
            'price_category' => 'Дорогой',
            'price' => '500'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 9,
            'price_category' => 'Стандарт',
            'price' => '350'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 9,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 10,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 11,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 12,
            'price_category' => 'Дорогой',
            'price' => '500'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 13,
            'price_category' => 'Стандарт',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 14,
            'price_category' => 'Стандарт',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 15,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 16,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 17,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 18,
            'price_category' => 'Дешевый',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 18,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 19,
            'price_category' => 'Дешевый',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 19,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 20,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 32,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 21,
            'price_category' => 'Дешевый',
            'price' => '350'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 21,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 22,
            'price_category' => 'Стандарт',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 23,
            'price_category' => 'Дешевый',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 23,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 24,
            'price_category' => 'Дешевый',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 24,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 25,
            'price_category' => 'Дорогой',
            'price' => '550'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 26,
            'price_category' => 'Стандарт',
            'price' => '350'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 26,
            'price_category' => 'Дорогой',
            'price' => '650'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 27,
            'price_category' => 'Дешевый',
            'price' => '250'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 27,
            'price_category' => 'Стандарт',
            'price' => '350'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 28,
            'price_category' => 'Дорогой',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 29,
            'price_category' => 'Дорогой',
            'price' => '650'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 30,
            'price_category' => 'Стандарт',
            'price' => '450'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 31,
            'price_category' => 'Дорогой',
            'price' => '650'

        ];
        $this->db->table('price')->insert($data);

        $data = [
            'id_session' => 32,
            'price_category' => 'Дорогой',
            'price' => '750'

        ];
        $this->db->table('price')->insert($data);
    }
}