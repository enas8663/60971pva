<?php

return [
    'film_create_success' => 'Фильм создан успешно',
    'film_update_success' => 'Фильм обновлен успешно',
    'film_delete_success' => 'Фильм удален успешно',
    'admin_permission_needed' => 'Требуются полномочия администратора'
];