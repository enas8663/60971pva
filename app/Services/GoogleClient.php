<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('1020256096548-s3thic3mns7pdlhm3snd7v390jdo1c8l.apps.googleusercontent.com');
        $this->google_client->setClientSecret('G1n2EJDfKX-y8B-rWQTArgnk');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}