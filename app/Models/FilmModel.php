<?php namespace App\Models;
use CodeIgniter\Model;
class FilmModel extends Model
{
    protected $table = 'film'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'time', 'picture_url', 'userid'];
    public function getFilm($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getFilmsWithUser($id = null, $search = '')
    {
        $builder = $this->select('film.id, name, time, film.picture_url, username, email')
            ->join('users','film.userid = users.id')
            ->like('name', $search,'both', null, true)
            ->orlike('email',$search,'both',null,true);

        if (!is_null($id))
        {
            return $builder->where(['film.id' => $id])->first();
        }
        return $builder;
    }
}