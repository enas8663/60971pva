<?php

namespace App\Models;
use CodeIgniter\Model;

class SessionModel extends Model
{
    protected $table = 'session'; //таблица, связанная с моделью
    protected $allowedFields = ['id_hall', 'id_film', 'date'];
    public function getSessions($id)
    {
        //return $this->where(['id_film' => intval($id)])->findAll();
        //return $this->like('id_film', $id,'both', null, true);

        return $this->select('session.id, id_hall, id_film, date, name')
            ->join('hall','id_hall = hall.id')
            ->where(['id_film' => $id])->findAll();
    }

    public function getFilmsWithUser($id = null, $search = '')
    {
        $builder = $this->select('film.id, name, time, film.picture_url, username, email')
            ->join('users','film.userid = users.id')
            ->like('name', $search,'both', null, true)
            ->orlike('email',$search,'both',null,true);

        if (!is_null($id))
        {
            return $builder->where(['film.id' => $id])->first();
        }
        return $builder;
    }
}