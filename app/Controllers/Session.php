<?php

namespace App\Controllers;

use App\Models\SessionModel;

class Session extends BaseController
{
    public function store() {
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id_film' => 'required',
                'id_hall'  => 'required',
                'date'  => 'required',
                'time'  => 'required',
            ]))
        {
            $model = new SessionModel();

            $data = [
                'id_film' => $this->request->getPost('id_film'),
                'id_hall' => $this->request->getPost('id_hall'),
                'date' => $this->request->getPost('date') . ' ' . $this->request->getPost('time'),
            ];

            $model->save($data);
            session()->setFlashdata('message', lang('Film.film_create_success'));
            return redirect()->to('/film/view/'.$this->request->getPost('id_film'));
        }
        else return redirect()->to('/film/view/'.$this->request->getPost('id_film'))->withInput();
    }

    public function update() {

    }

    public function delete($id) {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new SessionModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Film.film_delete_success'));
        return redirect()->to('/film');
    }
}