<?php namespace App\Controllers;

use App\Models\FilmModel;
use App\Models\SessionModel;
use Aws\S3\S3Client;

class Film extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $data ['film'] = $model->getFilm();
        echo view('film/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        helper(['form','url']);

        $model = new FilmModel();
        $sessions_model = new SessionModel();
        $data ['film'] = $model->getFilm($id);
        $data ['film_sessions'] = $sessions_model->getSessions($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('film/view', $this->withIon($data));
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new FilmModel();
            $data['films'] = $model->getFilmsWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;

            echo view('film/view_all_with_user', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Film.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('film/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[1]|max_length[255]',
                'time'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new FilmModel();

            $data = [
                'name' => $this->request->getPost('name'),
                'time' => $this->request->getPost('time'),
                'userid' => $this->request->getPost('userid'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);
            session()->setFlashdata('message', lang('Film.film_create_success'));
            return redirect()->to('/film');
        }
        else
        {
            return redirect()->to('/film/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();

        helper(['form']);
        $data ['film'] = $model->getFilm($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('film/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/film/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[1]|max_length[255]',
                'time'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new FilmModel();

            $data = [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'time' => $this->request->getPost('time'),
                'userid' => $this->request->getPost('userid'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);
            session()->setFlashdata('message', lang('Film.film_update_success'));

            return redirect()->to('/film');
        }
        else
        {
            return redirect()->to('/film/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Film.film_delete_success'));
        return redirect()->to('/film');
    }
}