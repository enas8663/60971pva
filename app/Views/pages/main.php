<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/860/860349.svg" alt="" width="72" height="72"><h1 class="display-4">Films</h1>
    <p class="lead">Это приложение поможет создавать каталог фильмов и составлять расписание сеансов.</p>
    <?php if (! $ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
