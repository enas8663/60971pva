<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('film/store'); ?>
        <input type="hidden" name="userid" value="<?= $ionAuth->user()->row()->id ?>">

            <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                value="<?= old('name')?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('name') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="time">Продолжительность</label>
                <input type="time" class="form-control <?= ($validation->hasError('time')) ? 'is-invalid' : ''; ?>" name="time"
                value="<?= old('time')?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('time') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="picture">Изображение</label>
                <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
            </div>
        </form>
    </div>
<?= $this->endSection() ?>