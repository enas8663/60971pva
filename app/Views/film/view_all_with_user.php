<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <div class="d-flex justify-content-between mb-2">
            <?= $pager->links('group1', 'my_page') ?>
            <?= form_open('film/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('film/viewAllWithUsers',['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Фильм/email автора записи" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>
        <?php if (!empty($films) && is_array($films)) : ?>
            <h2>Все фильмы:</h2>
            <table class="table table-striped">
                <thead>
                <th scope="col">Обложка</th>
                <th scope="col">Название</th>
                <th scope="col">Email создателя</th>
                <th scope="col">Длительность</th>
                <th scope="col">Управление</th>

                </thead>
                <tbody>
                <?php foreach ($films as $item): ?>
                    <tr>
                        <td>
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1179/1179069.svg" alt="<?= esc($item['name']); ?>">
                            <?php else:?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" alt="<?= esc($item['name']); ?>">
                            <?php endif ?>
                        </td>
                        <td><?= esc($item['name']); ?></td>
                        <td><?= esc($item['email']); ?></td>
                        <td><?= esc($item['time']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/film/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/film/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/film/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <p>Фильмы не найдены</p>
                <a class="btn btn-secondary btn-lg" href="<?= base_url()?>/film/create"><span class="fas fa-video" style="color:white"></span>&nbsp;&nbsp;Создать фильм</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>