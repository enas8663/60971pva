<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все фильмы</h2>

        <?php if (!empty($film) && is_array($film)) : ?>
        <div class="row">
            <?php foreach ($film as $item): ?>
            <div class="col col-md-4 col-sm-6 col-10">
                <div class="card" style="margin: 1em 0">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <img height="450" src="https://www.flaticon.com/svg/static/icons/svg/1179/1179069.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php else:?>
                        <img height="450" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php endif ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <p>Продолжительность: <?= esc($item['time']); ?></p>
                        <a href="<?= base_url()?>/film/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php else : ?>
            <p>Невозможно найти фильмы.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>