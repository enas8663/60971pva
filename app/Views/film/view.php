<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($film)) : ?>
            <div class="row" style="margin-bottom: 1.5em">
                <div class="card col-md-12">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center" style="margin: .5em 0">
                            <?php if (is_null($film['picture_url'])) : ?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1179/1179069.svg" class="card-img" alt="<?= esc($film['name']); ?>">
                            <?php else:?>
                                <img  src="<?= esc($film['picture_url']); ?>" class="card-img" alt="<?= esc($film['name']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($film['name']); ?></h5>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0">Продолжительность:</div>
                                    <div class="text-muted"><?= esc($film['time']); ?></div>
                                </div>
                                <hr>
                                <div>
                                    <?php if ($film['userid'] == $ionAuth->user()->row()->id) : ?>
                                        <a class="btn btn-primary" href="<?= base_url()?>/film/edit/<?= esc($film['id']); ?>">Редактировать</a>
                                        <a class="btn btn-danger" href="<?= base_url()?>/film/delete/<?= esc($film['id']); ?>">Удалить</a>
                                    <?php else:?>
                                        <p>Вы должны быть автором данной записи чтобы редактировать ее!</p>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($film['userid'] == $ionAuth->user()->row()->id) : ?>
                <?php echo form_open('session/store', ['class' => 'row']); ?>
                    <input type="hidden" name="id_film" value="<?= $film["id"] ?>">

                    <div class="form-group col">
                        <label for="date">Дата сеанса</label>
                        <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>" name="date" value="<?= old('date')?>">
                    </div>
                    <div class="form-group col">
                        <label for="time">Время сеанса</label>
                        <input type="time" class="form-control <?= ($validation->hasError('time')) ? 'is-invalid' : ''; ?>" name="time" value="<?= old('time')?>">
                    </div>
                    <div class="form-group col">
                        <label for="id_hall">Зал</label>
                        <select class="form-select form-control <?= ($validation->hasError('id_hall')) ? 'is-invalid' : ''; ?>" aria-label="Default select example" name="id_hall" value="<?= old('id_hall')?>">
                            <option selected disabled>Зал</option>
                            <option value="1">зал 1</option>
                            <option value="2">зал 2</option>
                            <option value="3">зал 3</option>
                            <option value="4">зал 4</option>
                            <option value="5">бол. зал</option>
                            <option value="6">4DX</option>
                        </select>
                    </div>

                    <div class="form-group col">
                        <button type="submit" class="btn btn-success" name="submit" style="position: absolute; bottom: 0">Добавить сеанс</button>
                    </div>
                <?php echo form_close();?>
            <?php endif ?>
            <div class="row" style="margin-top: 1.5em">
                <table class="table table-striped">
                    <thead>
                    <th scope="col">Дата</th>
                    <th scope="col">Зал</th>
                    </thead>
                    <tbody>
                    <?php if (!empty($film_sessions)) : ?>
                        <?php foreach ($film_sessions as $item): ?>
                            <tr>
                                <td><?= esc($item['date']); ?></td>
                                <td><?= esc($item['name']); ?></td>
                                <?php if ($film['userid'] == $ionAuth->user()->row()->id) : ?>
                                    <td><a class="btn btn-danger" href="<?= base_url()?>/session/delete/<?= esc($item['id']); ?>">Удалить</a></td
                                <?php endif ?>
                            </tr>
                        <?php endforeach; ?>
                    <?php else:?>
                        <p>Нет доступных сеансов</p>
                    <?php endif ?>
                    </tbody>
                </table>
            </div>
        <?php else : ?>
            <p>Фильм не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>