-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 28 2020 г., 03:02
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60971pva`
--

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE `film` (
  `id` int NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'наименование',
  `time` time NOT NULL COMMENT 'продолжительность'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `name`, `time`) VALUES
(1, 'Семейка Крудс: новоселье', '01:35:00'),
(2, 'Обратная связь', '01:37:00'),
(3, 'Звонок. Последняя глава', '01:39:00'),
(4, 'Проклятие Лауры. Завещание', '01:37:00'),
(5, 'Брешь', '01:32:00'),
(6, 'Про Лёлю и Миньку', '01:25:00'),
(7, 'Самый Новый год!', '01:20:00'),
(8, 'Серебряные коньки', '02:16:00'),
(9, 'Неадекватные люди 2', '02:15:00'),
(10, 'Творцы снов', '01:21:00');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `hall`
--

CREATE TABLE `hall` (
  `id` int NOT NULL COMMENT 'id зала',
  `name` varchar(255) NOT NULL COMMENT 'наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `hall`
--

INSERT INTO `hall` (`id`, `name`) VALUES
(1, 'зал 1'),
(2, 'зал 2'),
(3, 'зал 3'),
(4, 'зал 4'),
(5, 'бол. зал'),
(6, '4DX');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `place`
--

CREATE TABLE `place` (
  `id` int NOT NULL COMMENT 'id',
  `id_hall` int NOT NULL COMMENT 'id зала',
  `row` int NOT NULL COMMENT 'ряд',
  `count` int NOT NULL COMMENT 'номер',
  `price_category` varchar(255) NOT NULL COMMENT 'ценовая категория'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `place`
--

INSERT INTO `place` (`id`, `id_hall`, `row`, `count`, `price_category`) VALUES
(1, 1, 1, 1, 'Стандарт'),
(2, 1, 1, 2, 'Стандарт'),
(3, 1, 1, 3, 'Стандарт'),
(4, 1, 1, 4, 'Стандарт'),
(5, 1, 1, 5, 'Стандарт'),
(6, 1, 2, 6, 'Стандарт'),
(7, 1, 2, 7, 'Стандарт'),
(8, 1, 2, 8, 'Стандарт'),
(9, 1, 2, 9, 'Стандарт'),
(10, 1, 2, 10, 'Стандарт'),
(11, 1, 3, 11, 'Дорогой'),
(12, 1, 3, 12, 'Дорогой'),
(13, 1, 3, 13, 'Дорогой'),
(14, 1, 3, 14, 'Дорогой'),
(15, 1, 3, 15, 'Дорогой'),
(16, 2, 1, 1, 'Дешевый'),
(17, 2, 1, 2, 'Дешевый'),
(18, 2, 1, 3, 'Дешевый'),
(19, 2, 1, 4, 'Дешевый'),
(20, 2, 1, 5, 'Дешевый'),
(21, 2, 2, 6, 'Стандарт'),
(22, 2, 2, 7, 'Стандарт'),
(23, 2, 2, 8, 'Стандарт'),
(24, 2, 2, 9, 'Стандарт'),
(25, 2, 2, 10, 'Стандарт'),
(26, 2, 3, 11, 'Стандарт'),
(27, 2, 3, 12, 'Стандарт'),
(28, 2, 3, 13, 'Стандарт'),
(29, 2, 3, 14, 'Стандарт'),
(30, 2, 3, 15, 'Стандарт'),
(31, 3, 1, 1, 'Стандарт'),
(32, 3, 1, 2, 'Стандарт'),
(33, 3, 1, 3, 'Стандарт'),
(34, 3, 1, 4, 'Стандарт'),
(35, 3, 1, 5, 'Стандарт'),
(36, 3, 2, 6, 'Стандарт'),
(37, 3, 2, 7, 'Стандарт'),
(38, 3, 2, 8, 'Стандарт'),
(39, 3, 2, 9, 'Стандарт'),
(40, 3, 2, 10, 'Стандарт'),
(41, 3, 3, 11, 'Стандарт'),
(42, 3, 3, 12, 'Стандарт'),
(43, 3, 3, 13, 'Стандарт'),
(44, 3, 3, 14, 'Стандарт'),
(45, 3, 3, 15, 'Стандарт'),
(46, 4, 1, 1, 'Дорогой'),
(47, 4, 1, 2, 'Дорогой'),
(48, 4, 1, 3, 'Дорогой'),
(49, 4, 1, 4, 'Дорогой'),
(50, 4, 1, 5, 'Дорогой'),
(51, 4, 2, 6, 'Дорогой'),
(52, 4, 2, 7, 'Дорогой'),
(53, 4, 2, 8, 'Дорогой'),
(54, 4, 2, 9, 'Дорогой'),
(55, 4, 2, 10, 'Дорогой'),
(56, 5, 1, 1, 'Дешевый'),
(57, 5, 1, 2, 'Дешевый'),
(58, 5, 1, 3, 'Дешевый'),
(59, 5, 1, 4, 'Дешевый'),
(60, 5, 1, 5, 'Дешевый'),
(61, 5, 2, 6, 'Стандарт'),
(62, 5, 2, 7, 'Стандарт'),
(63, 5, 2, 8, 'Стандарт'),
(64, 5, 2, 9, 'Стандарт'),
(65, 5, 2, 10, 'Стандарт'),
(66, 5, 3, 11, 'Стандарт'),
(67, 5, 3, 12, 'Стандарт'),
(68, 5, 3, 13, 'Стандарт'),
(69, 5, 3, 14, 'Стандарт'),
(70, 5, 3, 15, 'Стандарт'),
(71, 5, 4, 16, 'Стандарт'),
(72, 5, 4, 17, 'Стандарт'),
(73, 5, 4, 18, 'Стандарт'),
(74, 5, 4, 19, 'Стандарт'),
(75, 5, 4, 20, 'Стандарт'),
(76, 6, 1, 1, 'Дорогой'),
(77, 6, 1, 2, 'Дорогой'),
(78, 6, 1, 3, 'Дорогой'),
(79, 6, 1, 4, 'Дорогой'),
(80, 6, 1, 5, 'Дорогой'),
(81, 6, 2, 6, 'Дорогой'),
(82, 6, 2, 7, 'Дорогой'),
(83, 6, 2, 8, 'Дорогой'),
(84, 6, 2, 9, 'Дорогой'),
(85, 6, 2, 10, 'Дорогой');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

CREATE TABLE `price` (
  `id` int NOT NULL COMMENT 'id',
  `id_session` int NOT NULL COMMENT 'id сеанса',
  `price_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'ценовая категория',
  `price` float NOT NULL COMMENT 'цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `price`
--

INSERT INTO `price` (`id`, `id_session`, `price_category`, `price`) VALUES
(1, 1, 'Дешевый', 100),
(2, 1, 'Дорогой', 200),
(3, 2, 'Дешевый', 100),
(4, 2, 'Стандарт', 250),
(5, 3, 'Дорогой', 500),
(6, 4, 'Дорогой', 600),
(7, 5, 'Стандарт', 350),
(8, 6, 'Дорогой', 700),
(9, 7, 'Стандарт', 450),
(10, 7, 'Дорогой', 650),
(11, 8, 'Дорогой', 500),
(12, 9, 'Стандарт', 350),
(13, 9, 'Дорогой', 550),
(14, 10, 'Дорогой', 550),
(15, 11, 'Дорогой', 450),
(16, 12, 'Дорогой', 500),
(17, 13, 'Стандарт', 450),
(18, 14, 'Стандарт', 450),
(19, 15, 'Дорогой', 550),
(20, 16, 'Дорогой', 450),
(21, 17, 'Дорогой', 450),
(22, 18, 'Дешевый', 250),
(23, 18, 'Дорогой', 550),
(24, 19, 'Дешевый', 250),
(25, 19, 'Дорогой', 550),
(26, 20, 'Дорогой', 450),
(27, 32, 'Дорогой', 550),
(28, 21, 'Дешевый', 350),
(29, 21, 'Дорогой', 550),
(30, 22, 'Стандарт', 250),
(31, 23, 'Дешевый', 250),
(32, 23, 'Дорогой', 450),
(33, 24, 'Дешевый', 250),
(34, 24, 'Дорогой', 450),
(35, 25, 'Дорогой', 550),
(36, 26, 'Стандарт', 350),
(37, 26, 'Дорогой', 650),
(38, 27, 'Дешевый', 250),
(39, 27, 'Стандарт', 350),
(40, 28, 'Дорогой', 450),
(41, 29, 'Дорогой', 650),
(42, 30, 'Стандарт', 450),
(43, 31, 'Дорогой', 650),
(44, 32, 'Дорогой', 750);

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE `session` (
  `id` int NOT NULL COMMENT 'id',
  `id_hall` int NOT NULL COMMENT 'id зала',
  `id_film` int NOT NULL COMMENT 'id фильм',
  `date` datetime NOT NULL COMMENT 'дата и время начала'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id`, `id_hall`, `id_film`, `date`) VALUES
(1, 1, 1, '2020-12-20 10:35:00'),
(2, 2, 1, '2020-12-20 12:35:00'),
(3, 5, 1, '2020-12-21 08:37:00'),
(4, 6, 1, '2020-12-21 13:12:00'),
(5, 3, 2, '2020-12-20 12:44:00'),
(6, 4, 2, '2020-12-27 14:39:00'),
(7, 1, 2, '2020-12-28 15:40:00'),
(8, 6, 2, '2020-12-20 11:40:00'),
(9, 1, 3, '2020-12-24 09:30:00'),
(10, 6, 3, '2020-12-24 10:40:00'),
(11, 5, 3, '2020-12-23 12:18:00'),
(12, 4, 3, '2020-12-26 09:43:00'),
(13, 3, 4, '2020-12-22 12:30:00'),
(14, 3, 4, '2020-12-22 14:45:00'),
(15, 6, 4, '2020-12-26 13:46:00'),
(16, 5, 4, '2020-12-20 15:03:00'),
(17, 4, 5, '2021-01-13 09:00:00'),
(18, 2, 5, '2021-01-07 10:35:00'),
(19, 2, 5, '2021-01-11 13:35:00'),
(20, 4, 5, '2020-12-30 12:45:00'),
(21, 2, 6, '2021-01-11 07:10:00'),
(22, 3, 6, '2021-01-10 11:00:00'),
(23, 2, 7, '2021-01-12 09:15:00'),
(24, 2, 7, '2021-01-12 14:50:00'),
(25, 5, 8, '2020-12-17 07:00:00'),
(26, 1, 8, '2020-12-17 20:17:00'),
(27, 2, 9, '2020-12-18 22:00:00'),
(28, 6, 9, '2020-12-18 23:00:00'),
(29, 5, 10, '2020-12-25 19:00:00'),
(30, 3, 10, '2020-12-25 21:00:00'),
(31, 5, 8, '2021-01-16 13:00:00'),
(32, 6, 3, '2021-01-03 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `ticket`
--

CREATE TABLE `ticket` (
  `id` int NOT NULL COMMENT 'id',
  `id_session` int NOT NULL COMMENT 'id сеанса',
  `id_seat` int NOT NULL COMMENT 'id места',
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'ФИО'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `ticket`
--

INSERT INTO `ticket` (`id`, `id_session`, `id_seat`, `full_name`) VALUES
(1, 1, 37, 'Иванов Иван Иванович'),
(2, 1, 38, 'Иванов Иван Петрович'),
(3, 1, 19, 'Иванов Иван Константинович'),
(4, 1, 13, 'Иванов Иван Александрович'),
(5, 1, 60, 'Иванов Иван Сергеевич'),
(6, 2, 76, 'Иванов Петр Иванович'),
(7, 2, 19, 'Иванов Петр Сергеевич'),
(8, 2, 56, 'Иванов Петр Петрович'),
(9, 2, 46, 'Иванов Петр Александрович'),
(10, 2, 13, 'Иванов Петр Николаевич'),
(11, 3, 59, 'Иванов Сергей Иванович'),
(12, 3, 56, 'Иванов Сергей Сергеевич'),
(13, 7, 57, 'Иванов Сергей Иванович'),
(14, 7, 12, 'Иванов Сергей Алексеевич'),
(15, 13, 14, 'Иванов Константин Иванович'),
(16, 26, 59, 'Иванов Иван Иванович'),
(17, 32, 29, 'Иванов Сергей Иванович'),
(18, 26, 12, 'Иванов Петр Иванович'),
(19, 23, 82, 'Иванов Сергей Иванович'),
(20, 8, 4, 'Иванов Сергей Сергеевич');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$bjXTtHZ7CiZa5voB6jEo1.zTHqeWKnrQ9dq2ZLg3GIjRqibxUeinW', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1609106047, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', 'vadim8663@mail.ru', '$2y$10$LmeOwqaNcro8B5e6ACBsB.Kvj/RMKF36nX33kg69kSaqoLIUVU2ku', 'vadim8663@mail.ru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1608646692, 1608662576, 1, 'Potapov', 'Vadim', '-', '880053535'),
(3, '127.0.0.1', 'vadim86633@mail.ru', '$2y$10$0yO0fuvc68.8UMF/KlmXeeob1VfWIjytkTVQ6bh603cwWiPaRmZVa', 'vadim86633@mail.ru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1608653890, 1608663386, 1, 'Вадим', 'Потапов', '-', '88005553535');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hall`
--
ALTER TABLE `hall`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_hall` (`id_hall`);

--
-- Индексы таблицы `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_session` (`id_session`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_hall` (`id_hall`),
  ADD KEY `id_film` (`id_film`);

--
-- Индексы таблицы `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_session` (`id_session`),
  ADD KEY `id_seat` (`id_seat`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `film`
--
ALTER TABLE `film`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `hall`
--
ALTER TABLE `hall`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id зала', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `place`
--
ALTER TABLE `place`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT для таблицы `price`
--
ALTER TABLE `price`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT для таблицы `session`
--
ALTER TABLE `session`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `place`
--
ALTER TABLE `place`
  ADD CONSTRAINT `place_ibfk_1` FOREIGN KEY (`id_hall`) REFERENCES `hall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `price_ibfk_1` FOREIGN KEY (`id_session`) REFERENCES `session` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_2` FOREIGN KEY (`id_hall`) REFERENCES `hall` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `session_ibfk_3` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`id_seat`) REFERENCES `place` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`id_session`) REFERENCES `session` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
